package net.bpixel.base.api.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserModel {

	private String name;
	private String email;
	private String password;
	
}
