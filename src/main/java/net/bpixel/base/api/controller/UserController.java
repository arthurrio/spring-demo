package net.bpixel.base.api.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.bpixel.base.api.model.UserModel;

@RestController("user")
public class UserController {
	
	@Value("${teste}")
	String x;

	@RequestMapping
	public UserModel get() {
		
		return UserModel.builder()
				.email("email@email.com")
				.name(x)
				.password("12321321312").build();
	}

}
